module gitlab.com/airhorndevelopment/go-muxtools

go 1.15

require (
	github.com/creack/pty v1.1.11 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/kr/pty v1.1.8 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	gitlab.com/airhorndevelopment/metalog v0.0.0-20210315091544-b5760ed3863d
	golang.org/x/sys v0.0.0-20210313110737-8e9fff1a3a18 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
