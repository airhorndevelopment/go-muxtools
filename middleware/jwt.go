package middleware

import (
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/airhorndevelopment/metalog/metalog"
	"net/http"
)

var logger = metalog.GetLogger("go-muxtools")

func BuildJWTAuthMiddleware(apiKey string) func(handler http.Handler) http.Handler {
	return func (next http.Handler) http.Handler {
		return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
			if request.Method == http.MethodOptions {
				next.ServeHTTP(writer, request)
				return
			}

			providedKey := request.Header.Get("Authorization")
			token, err := jwt.Parse(providedKey, func(token *jwt.Token) (interface{}, error) {
				return []byte(apiKey), nil
			})

			if err != nil {
				logger.Errorf("Error parsing token from request: %s", providedKey)
				http.Error(writer, "Unauthorized", http.StatusUnauthorized)
				return
			}

			if !token.Valid {
				http.Error(writer, "Unauthorized", http.StatusUnauthorized)
				return
			} else {
				next.ServeHTTP(writer, request)
			}
		})
	}
}
