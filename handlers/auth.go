package handlers

import (
	"crypto/subtle"
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"github.com/sirupsen/logrus"
	"gitlab.com/airhorndevelopment/metalog/metalog"
	"net/http"
	"time"
)

var logger = metalog.GetLogger("go-muxtools")

func BuildGetTokenHttpHandler(username string, password string, apiKey string, expires time.Duration) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if subtle.ConstantTimeCompare([]byte(r.FormValue("username")), []byte(username)) == 1 &&
			subtle.ConstantTimeCompare([]byte(r.FormValue("password")), []byte(password)) == 1 {

			token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
				"exp": expires,
			})

			tokenString, err := token.SignedString([]byte(apiKey))
			if err != nil {
				logger.WithFields(logrus.Fields{
					"exception": err,
				}).Error("Failed to sign token")
			}

			_ = json.NewEncoder(w).Encode(struct {
				Token string `json:"token"`
			}{
				Token: tokenString,
			})
			if err != nil {
				logger.WithFields(logrus.Fields{
					"exception": err.Error(),
				}).Error("Failed to write token to response writer")
			} else {
				logger.WithFields(logrus.Fields{
					"remote": r.RemoteAddr,
					"expires": time.Now().Add(expires),
				}).Info("Successful login")
			}
		}
	}
}
